import { JazzMusician } from "./JazzMusician";
import { Musician } from "./Musician";
import { RockStar } from "./RockStar";

const mingus = new JazzMusician("Charles", "Mingus", 57);
const wampas = new RockStar("Didier", "Wampas", 54);
const musicianList: Musician[] = [mingus, wampas];

type MusicianData = Omit <Musician, "style" | "albums">
type OnlyFirstAndLastName = Pick <Musician, "firstName" | "lastName" | "age">