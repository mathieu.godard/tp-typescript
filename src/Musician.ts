import { Album } from './Album'

export enum Music {
    JAZZ,
    ROCK
}

interface IMusician {
    addAlbum(album: Album): void;
}

export class Musician implements IMusician {

    private _style: Music | undefined;
    private _albums: Album[] = [];

    constructor(
        public firstName: string, 
        public lastName: string, 
        public age: number 
        ) {
    }

    get style() {
        return this._style;
    }

    set style(newStyle) {
        this._style = newStyle;
    }

    toString(): string {
        const theStyle = this._style !== undefined ? Music[this._style] : "music";
        return `${this.firstName} ${this.lastName} plays ${theStyle}`
    }

    get albums(): Album[] {
        return this._albums;
    }

    addAlbum(album: Album) {
        this._albums.push(album);
    }
}