
import display from "./Display";
import { JazzMusician } from "./JazzMusician";
import { Music, Musician } from "./Musician";
import { RockStar } from "./RockStar";
import SpyInstance = jest.SpyInstance;

describe('TP3', () => {

    let spy: SpyInstance;

    beforeEach(() => {
        spy = jest.spyOn(console, 'log');
        spy.mockClear();
    });

    it('JazzMusician properties testing', () => {
        const musician = new JazzMusician('Miles', 'Davis', 89);
        expect(musician.style).toBe(Music.JAZZ);
    });

    it('JazzMusician toString method testing', () => {
        const musician = new JazzMusician('Miles', 'Davis', 89);
        expect(musician.toString()).toBe('Miles Davis plays JAZZ');
    });

    it('display method testing with musicians', () => {
        const musicians: Musician[] = [new JazzMusician('Miles', 'Davis', 89), new RockStar('Mick', 'Jagger', 72)];

        display(musicians);

        expect(spy).toBeCalledTimes(2);
        expect(spy.mock.calls[0][0]).toBe('Miles Davis plays JAZZ');
        expect(spy.mock.calls[1][0]).toBe('Mick Jagger plays ROCK');
    });

});