import { Album } from "./Album";
import display from "./Display";
import { JazzMusician } from "./JazzMusician";
import { log } from "./Log";
import { Musician } from "./Musician";
import { RockStar } from "./RockStar";

log('Bienvenue dans ma première application TypeScript.')
const mingus = new JazzMusician("Charles", "Mingus", 57);
const wampas = new RockStar("Didier", "Wampas", 54);
const musicianList: Musician[] = [mingus, wampas];

mingus.addAlbum(new Album('Tijuana Moods'));
mingus.addAlbum(new Album('Oh Yeah !'));
wampas.addAlbum(new Album('Les Wampas sont la preuve que Dieu existe'));
wampas.addAlbum(new Album('Chauds, sales et humides'));
wampas.addAlbum(new Album('Les Wampas font la gueule'));

log(musicianList);

musicianList.forEach(element => {
    display(element.albums)
});

musicianList.forEach(Object => {
    if (Object instanceof RockStar) {
        Object.shout();
    }
    if (Object instanceof JazzMusician) {
        Object.swing();
    }
});