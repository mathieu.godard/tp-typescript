export interface ElementToString {
    toString(): string;
}