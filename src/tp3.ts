interface IMusician {
    addAlbum(album: Album): void;
}

interface ElementToString {
    toString(): string;
}

export function display<T extends ElementToString>(obj: T[]) {
    obj.forEach( item => {
        console.log(item.toString())
    });
}

export enum Music {
    JAZZ,
    ROCK
}


export class Album {

    constructor(public title: string) {}

    toString(): string {
        return `${this.title}`;
    }
}


export class Musician implements IMusician {

    private _style: Music | undefined;
    private _albums: Album[] = [];

    constructor(
        public firstName: string, 
        public lastName: string, 
        public age: number 
        ) {
    }

    get style() {
        return this._style;
    }

    set style(newStyle) {
        this._style = newStyle;
    }

    toString(): string {
        const theStyle = this._style !== undefined ? Music[this._style] : "music";
        return `${this.firstName} ${this.lastName} plays ${theStyle}`
    }

    get albums(): Album[] {
        return this._albums;
    }

    addAlbum(album: Album) {
        this._albums.push(album);
    }
}


export class JazzMusician extends Musician {

    constructor(
    public firstName: string, 
    public lastName: string, 
    public age: number) {
        super(firstName, lastName, age);
        this.style = Music.JAZZ;
    }
}


export class RockStar extends Musician {

    constructor(
        public firstName: string, 
        public lastName: string, 
        public age: number) {
            super(firstName, lastName, age);
            this.style = Music.ROCK;
    }
}