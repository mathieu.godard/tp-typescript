export class Album {

    constructor(public title: string) {}

    toString(): string {
        return `${this.title}`;
    }
}