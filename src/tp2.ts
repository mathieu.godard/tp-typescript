export function returnPeopleAndLength(people: string[] = ['Miles', 'Mick']): [string, number][] {
    const myArray: [string, number][] = people.map((personn) => [personn, personn.length]);
    return myArray;
}

// Autre façon de l'écrire, éviter de stocker une variable pour rien :
// export function returnPeopleAndLength(people: string[] = ['Miles', 'Mick']): [string, number][] {
//     return people.map((personn) => [personn, personn.length]);
// }

export function displayPeopleAndLength(people?: string[], litteral?: boolean): void {
    if (litteral === false || litteral === undefined) {
        returnPeopleAndLength(people).forEach(([name, lengthName]) => {
            console.log(`${name} contient ${lengthName} caractères`);
        })
    }
    if (litteral === true) {
        returnPeopleAndLength(people).filter((personn) => personn.length <= 9).forEach((personn) => {
            console.log(`${personn[0]} contient ${NumberToString[personn[1]-1]} caractères`);
        });
    }
}

// export function displayPeopleAndLength(people?: string[], litteral?: boolean): void {
//     if (!litteral) {
//         returnPeopleAndLength(people).forEach(([name, lengthName]) => {
//             console.log(`${name} contient ${lengthName} caractères`);
//         })
//         return;
//     }
//     if (litteral) {
//         returnPeopleAndLength(people).filter((personn) => personn.length <= 9).forEach((personn) => {
//             console.log(`${personn[0]} contient ${NumberToString[personn[1]-1]} caractères`);
//         });
//     }
// }



enum NumberToString {
    un,
    deux,
    trois,
    quatre,
    cinq,
    six,
    sept,
    huit,
    neuf
}

// enum NumberToString {
//     un = 1,
//     deux = 2,
//     trois = 3,
//     quatre = 4,
//     cinq = 5,
//     six = 6,
//     sept = 7,
//     huit = 8,
//     neuf = 9
// }