import { log } from "./Log";
import { Music, Musician } from "./Musician";

export class RockStar extends Musician {

    constructor(
        public firstName: string, 
        public lastName: string, 
        public age: number) {
            super(firstName, lastName, age);
            this.style = Music.ROCK;
    }

    shout() {
        log("I'm shouting!")
    }
}