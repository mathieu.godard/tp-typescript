
import { Album } from "./Album";
import display from "./Display";
import { JazzMusician } from "./JazzMusician";
import { Music, Musician } from "./Musician";
import { RockStar } from "./RockStar";
import SpyInstance = jest.SpyInstance;

describe('TP3', () => {

    let spy: SpyInstance;

    beforeEach(() => {
        spy = jest.spyOn(console, 'log');
        spy.mockClear();
    });

    it('RockStar properties testing', () => {
        const musician = new RockStar('Mick', 'Jagger', 72);
        expect(musician.style).toBe(Music.ROCK);
    });

    it('RockStar toString method testing', () => {
        const musician = new RockStar('Mick', 'Jagger', 72);
        expect(musician.toString()).toBe('Mick Jagger plays ROCK');
    });

});