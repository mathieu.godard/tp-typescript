
import { Album } from "./Album";
import display from "./Display";
import SpyInstance = jest.SpyInstance;

describe('TP3', () => {

    let spy: SpyInstance;

    beforeEach(() => {
        spy = jest.spyOn(console, 'log');
        spy.mockClear();
    });

    it('Album properties testing', () => {
        const al = new Album('Kind Of Blue');
        expect(al.title).toBe('Kind Of Blue');
    });

    it('Album toString method testing', () => {
        const al = new Album('Kind Of Blue');
        expect(al.toString()).toBe('Kind Of Blue');
    });

    it('display method testing with albums', () => {
        const albums: Album[] = [new Album('Kind Of Blue'), new Album('Tutu')];

        display(albums);

        expect(spy).toBeCalledTimes(2);
        expect(spy.mock.calls[0][0]).toBe('Kind Of Blue');
        expect(spy.mock.calls[1][0]).toBe('Tutu');
    });

});