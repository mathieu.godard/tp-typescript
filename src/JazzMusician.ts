import { log } from "./Log";
import { Music, Musician } from "./Musician";

export class JazzMusician extends Musician {

    constructor(
    public firstName: string, 
    public lastName: string, 
    public age: number) {
        super(firstName, lastName, age);
        this.style = Music.JAZZ;
    }

    swing() {
        log("I'm swinging!")
    }
}