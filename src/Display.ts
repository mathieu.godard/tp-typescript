import { ElementToString } from './Utils';
import { log } from './Log';

function display<T extends ElementToString>(obj: T[]) {
    obj.forEach( item => {
        log(item.toString())
    });
}

export default display;