
import { Album } from "./Album";
import { Music, Musician } from "./Musician";
import SpyInstance = jest.SpyInstance;

describe('TP3', () => {

    let spy: SpyInstance;

    beforeEach(() => {
        spy = jest.spyOn(console, 'log');
        spy.mockClear();
    });

    it('Enum Music testing', () => {
        expect(Music[0]).toBe('JAZZ');
        expect(Music[1]).toBe('ROCK');
    });

    it('Musician properties testing', () => {
        const musician = new Musician('Miles', 'Davis', 89);
        expect(musician.firstName).toBe('Miles');
        expect(musician.lastName).toBe('Davis');
        expect(musician.age).toBe(89);
        expect(musician.albums).toHaveLength(0);
        expect(musician.style).toBeFalsy();
    });

    it('Musician addAlbum testing', () => {
        const musician = new Musician('Miles', 'Davis', 89);
        musician.addAlbum(new Album('Kind Of Blue'));
        musician.addAlbum(new Album('Tutu'));
        expect(musician.albums).toHaveLength(2);
    });

});